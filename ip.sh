#! /bin/bash

COUNT=0
for I in {1..254}
do
IP="192.168.1.$I"
C=`ping -c1 -w1 $IP|grep "1 received"`
if [ -n "$C" ]
 then 
echo -e "\033[36m$IP\033[0m" 
COUNT=`expr $COUNT + 1`
fi
done
echo -e "\033[32;1m COUNT = $COUNT\033[0m" 
