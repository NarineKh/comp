#! bin/bash
#function given out information on the display
function DISPLAY {
echo -e "\033[32m*****************************DISPLAY**************************\033[0m"
echo "`lspci | grep VGA`"
echo "`xrandr -q | grep -v "disconnected"`"
echo "`xwininfo -root | head -n12 | tail -n2`"
}
#information on the processor
function PROCESSOR {
echo -e "\033[34m**************************PROCESSOR**************************\033[0m"
PROCESS=0
WORD_PROCESS="processor"
PROC=`cat /proc/cpuinfo`
for PROCESS_COUNT in $PROC
 do
  if [ $PROCESS_COUNT == $WORD_PROCESS ]
  then
    PROCESS=`expr $PROCESS + 1`
  fi
done
echo processor "        :" $PROCESS
echo model_name "       :`cat /proc/cpuinfo | grep "model name"|cut -d":" -f2,3,4,5,6,7 |head -1`"
echo vendor_id "        :`cat /proc/cpuinfo | grep "vendor_id" |cut -d":" -f2| head -1`" 
}
#information on system
function SYSTEM {
echo -e "\033[35m**************************SYSTEM**************************\033[0m"
echo system name  "      :`cat /etc/*release* | grep "DISTRIB_DESCRIPTION"|cut -d"=" -f2`"
echo version of a core  ":`uname -r`" 
echo digit capacity     "   :`uname -m`" 
}
#information on memory
function MEMORY {
echo -e "\033[31m**************************MEMORY**************************\033[0m"
TOTAL="`free -h | grep "Mem"`"
echo total_memory ":" `echo $TOTAL |cut -d" " -f2`
echo used_memory " :" `echo $TOTAL |cut -d" " -f3`
echo free_memory " :" `echo $TOTAL |cut -d" " -f4`
}
#information on the hard drive
function HDD {
echo -e "\033[33m***************************HDD****************************\033[0m" 
echo "NAME     SIZE"
echo "`lsblk -o "NAME,SIZE" |grep "sd"`"
}
#information on a network
function NETWORK {
echo -e "\033[32m*************************NETWORK*************************\033[0m"
ETHERNET="eth0"
INTERNET=`ifconfig`
for ETHERNET1 in $INTERNET
do
  if [ $ETHERNET1 == $ETHERNET ]
  then
    echo "LAN  :YES"
  fi
done
WLAN_NET="wlan0"
for WLAN_NET1 in $INTERNET
do
  if [ $WLAN_NET1 == $WLAN_NET ]
  then
    echo "WIFI :YES"
  fi
done
}
#help
function HELP {
echo -e "\033[036m*******************************HELP****************************\033[0m"
echo 
echo "--all                              opens full information"
echo "-P                                 opens information on the processor"
echo "-N                                 opens information on a network"
echo "-S                                 opens information on system"
echo "-M                                 opens information on memory"
echo "-H                                 opens information on the hard drive"
echo "-D                                 opens information on the display"
echo "--help                             opens the help menu "
}
#depending on parametrs, call the appropriate function
if [ -z "$*" ]
then
   HELP
fi
for FUNCTION_CALL in $*
do
  if [ $FUNCTION_CALL = "--all" ]
  then
    DISPLAY
    PROCESSOR
    SYSTEM
    MEMORY
    HDD
    NETWORK
    HELP
   elif [ $FUNCTION_CALL = "-P" ]
     then
       PROCESSOR
   elif [ $FUNCTION_CALL = "-D" ]
     then
       DISPLAY
   elif [ $FUNCTION_CALL = "-S" ]
      then
       SYSTEM
   elif [ $FUNCTION_CALL = "-M" ]
      then
       MEMORY
   elif [ $FUNCTION_CALL = "-H" ]
      then
       HDD
   elif [ $FUNCTION_CALL = "-N" ]
      then
       NETWORK
   elif [ $FUNCTION_CALL = "--help" ]
      then
       HELP
    else
      clear
       HELP
  fi
done
